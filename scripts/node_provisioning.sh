#!/bin/bash

# #############################################################################
# Variables
# #############################################################################

PUPPET_PACKAGE="puppet6-release-bionic.deb"
PUPPET_AGENT_CONFIG_FILE="/etc/puppetlabs/puppet/puppet.conf"
MY_NODE_FQDN="$1"
MY_PUPPET_SERVER="puppetserver.example.org"
MY_PUPPET_SERVER_IP="10.0.0.10"

PUPPET="/opt/puppetlabs/bin/puppet"

IS_VAGRANT=0

# #############################################################################
# functions
# #############################################################################
function is_vagrant() {
  if [ -d "/vagrant" ]; then
    IS_VAGRANT=1
  fi
}

# Install Puppet agent package
function install_puppet_agent() {
  if [ ! -f "${PUPPET}" ]; then
    sudo wget --quiet "https://apt.puppetlabs.com/${PUPPET_PACKAGE}"
    sudo dpkg -i "${PUPPET_PACKAGE}"
    sudo apt-get update
    sudo apt-get install puppet-agent
  fi
}

# Register node to puppetserver
function puppetserver_register() {
  # Create config file if necessary
  if [ ! -f "${PUPPET_AGENT_CONFIG_FILE}" ]; then
    touch "${PUPPET_AGENT_CONFIG_FILE}"
  fi

  # If Vagrant, use IP rather than host name
  if [ "${IS_VAGRANT}" ]; then
    echo "${MY_PUPPET_SERVER_IP} ${MY_PUPPET_SERVER}" > /etc/hosts
  fi
  "${PUPPET}" config set server "${MY_PUPPET_SERVER}"
  "${PUPPET}" config set certname "${MY_NODE_FQDN}"
}

# #############################################################################
# main
# #############################################################################
install_puppet_agent
puppetserver_register
