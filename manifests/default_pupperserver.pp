class { 'puppet':
  server                      => true,
  server_foreman              => false,
  server_reports              => 'store',
  server_external_nodes       => '',
  server_puppetserver_version => '6.6.0',
}

# Add Gitlab.com SSH public key to root user
sshkey { 'gitlab.com':
  ensure => present,
  type   => 'ecdsa-sha2-nistp256',
  target => '/root/.ssh/known_hosts',
  key    =>
    'AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY='
  ,
}

# Generate SSH keys for user root
ssh_keygen { 'root':
  type     => 'ed25519',
  filename => '/root/.ssh/id_ed25519',
}

# Vagrant ONLY: DNS entries required for Firewall module to be run
file_line { 'entry_node':
  path => '/etc/hosts',
  line => '10.0.0.11	node.example.org node',
}
file_line { 'entry_grafana':
  path => '/etc/hosts',
  line => '10.0.0.12	grafana.example.org	grafana',
}
